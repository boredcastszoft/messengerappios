//
//  Message.swift
//  Boredcast
//
//  Created by Rudolf Dani on 2018. 10. 13..
//  Copyright © 2018. Rudolf Dani. All rights reserved.
//

import Foundation

class Message {
    
 
    private var _message: String!
    private var _sender: String!
    private var _receiver: String!
    private var _sentTime: CLong!

    

    
    var message: String {
        get {
            return _message
        }
    }
    
    var sender: String {
        get {
            return _sender
        }
    }
    
    var receiver: String {
        get {
            return _receiver
        }
    }
    
    var sentTime: CLong {
        get {
            return _sentTime
        }
    }
    
    init(message: String, sender: String, receiver: String, sentTime: CLong) {
        
        self._message = message
        self._sender = sender
        self._receiver = receiver
        self._sentTime = sentTime
    }
    
    
    
}
