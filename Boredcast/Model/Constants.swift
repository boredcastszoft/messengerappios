//
//  Constants.swift
//  Boredcast
//
//  Created by Rudolf Dani on 2018. 09. 24..
//  Copyright © 2018. Rudolf Dani. All rights reserved.
//

import Foundation

let BASEURL = "https://jsonplaceholder.typicode.com/posts/1"
let BASEURL2 = "https://pokeapi.co/api/v2/pokemon/1"
let URLFRIEND = "http://172.21.28.55:8182/friends"
let URLLOGIN = "http://172.21.28.55:8182/login"
let URLMESSAGE = "http://172.21.28.55:8182/message"
let URLLOGOUT = "http://172.21.28.55:8182/logout"
let URLREGISTER = "http://172.21.28.55:8182/register"
let URLMESSAGES = "http://172.21.28.55:8182/getmessages"
let URLGROUPS = "http://172.21.28.55:8182/getgroups"
let URLPOLLING = "http://172.21.28.55:8182/polling"


let MAINURL = "http://172.21.28.55:8182/"

typealias downloadCompleted = () -> ()
