//
//  AddFriendVC.swift
//  Boredcast
//
//  Created by Rudolf Dani on 2018. 11. 01..
//  Copyright © 2018. Rudolf Dani. All rights reserved.
//
import Alamofire
import UIKit

class AddFriendVC: UIViewController {

    @IBOutlet weak var addFriendLbl: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    
    @IBAction func dismissBtnPushed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
 
    @IBAction func addFriendBtnPushed(_ sender: Any) {
        
        let userName = UserDefaults.standard.object(forKey: "userName")
        
        let parameters: Parameters = [
            "userFriendName" : addFriendLbl.text!,
            "whoseFriend" : userName!
        ]
        
        Alamofire.request(MAINURL+"friendrequest", method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { (response) in
            print(response)
        }
        
    }
    
    @IBAction func cancelBtnPushed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
