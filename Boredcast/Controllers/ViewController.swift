//
//  ViewController.swift
//  Boredcast
//
//  Created by Rudolf Dani on 2018. 09. 22..
//  Copyright © 2018. Rudolf Dani. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {
    
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var userNameText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    
    var friends: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        
        if let userName = UserDefaults.standard.object(forKey: "userName") as? String {
            self.userNameText.text = userName
        }
        if let password = UserDefaults.standard.object(forKey: "password") as? String {
            self.passwordText.text = password
        }
        
    }

    var kek: Bool!
    
    @IBAction func loginBtnPushed(_ sender: Any) {
        
        UserDefaults.standard.set(userNameText.text, forKey: "userName")
        UserDefaults.standard.set(passwordText.text, forKey: "password")
        UserDefaults.standard.synchronize()
        
        let username = userNameText.text
        let password = passwordText.text
        
       // userName = userNameText.text
        
        if (username?.isEmpty)! || (password?.isEmpty)! {
            
            displayMessage(userMessage: "Mindket mezot ki kell tolteni!")
            return
        }
        
        
        self.logInRequest() {
            if self.kek == true{
            
                self.donloadFriendList {
                    print("letoltes")
                    self.performSegue(withIdentifier: "MessengerVC", sender: self.friends)
               }
                
            }
        }
            /*let MessengerVC = self.storyboard?.instantiateViewController(withIdentifier: "MessengerVC") as! MessengerVC
            self.present(MessengerVC, animated: true)*/
    }
        
    
    func displayMessage(userMessage:String) -> Void {
        DispatchQueue.main.async {
            
            let alertController = UIAlertController(title: "Alert", message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                DispatchQueue.main.async {
                    self.dismiss(animated: true, completion: nil)
                }
            }
            
            alertController.addAction(OKAction)
            self.present(alertController, animated: true)   	        
        }
    }
    
    func logInRequest (completed:@escaping downloadCompleted) {

        self.friends.removeAll()
        let username = userNameText.text!
        let password = passwordText.text!
        
        let parameters : Parameters = [
            "userName" : username,
            "password" : password
        ]
        
        
        let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        myActivityIndicator.center = self.view.center
        myActivityIndicator.hidesWhenStopped = false
        myActivityIndicator.startAnimating()
        self.view.addSubview(myActivityIndicator)
        
        Alamofire.request(MAINURL+"login", method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { (response) in
            print(response)

            if let dict = response.result.value as? Dictionary<String, AnyObject> {
                if let loginOK = dict["state"] as? String {
                    if loginOK == "wus" {
                        self.displayMessage(userMessage: "Wrong Username or Password!")
                        self.kek = false

                    }else if loginOK == "ok" {
                            
                        print("ok")
                        self.kek = true
                        completed()
     
                    }else if loginOK == "nop" {
                            
                        self.displayMessage(userMessage: "Nincs ilyen felahsznalo!")
                        self.kek = false
                            
                    }
                }
            }else {
                    
                self.displayMessage(userMessage: "Unable to connect!")
                self.kek = false
                return
            }
        }
        self.removeActivityIndcator(activityIndicator: myActivityIndicator)
        completed()
        
    }
    
    func removeActivityIndcator(activityIndicator: UIActivityIndicatorView) {
        
        DispatchQueue.main.async {
            activityIndicator.stopAnimating()
            activityIndicator.removeFromSuperview()
            
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let messengerVC = segue.destination as? MessengerVC else {return}
        messengerVC.friendList = friends
        
    }
    
    func donloadFriendList(completed:@escaping downloadCompleted) {
        
        let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        myActivityIndicator.center = self.view.center
        myActivityIndicator.hidesWhenStopped = false
        myActivityIndicator.startAnimating()
        self.view.addSubview(myActivityIndicator)
        
        /*Alamofire.request("https://jsonplaceholder.typicode.com/users").responseJSON() { (response) in
            
            if let dict = response.result.value as? [Dictionary<String, AnyObject>] {
                for x in 0..<dict.count {
                    
                    if let name = dict[x]["name"] as? String {
                        
                        self.friends.append(name)
  
                    }
                }
                completed()
                self.removeActivityIndcator(activityIndicator: myActivityIndicator)
            }
            
        }*/
     
        let parameters : Parameters = [
            "userName" : userNameText.text!
        ]
        
        print(parameters)
        Alamofire.request(MAINURL+"friends", method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { (response) in
            //print(response)
            if let dict = response.result.value as? [Dictionary<String, AnyObject>], dict.count>0{
                for x in 0..<dict.count {
                    if let friend =  dict[x]["userFriendName"] as? String {
                        
                        self.friends.append(friend)
                    }
                }
                
            }
            
            //self.removeActivityIndcator(activityIndicator: myActivityIndicator)
        }
        
        Alamofire.request(MAINURL+"getgroups", method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { (response) in
            print("Groupok:")
            print(response)
            if let dict = response.result.value as? [Dictionary<String, AnyObject>], dict.count>0{
                for x in 0..<dict.count {
                    if let group =  dict[x]["groupName"] as? String {
                        
                        self.friends.append(group)
                    }
                }
                
            }
            completed()
            self.removeActivityIndcator(activityIndicator: myActivityIndicator)
        }
        
    }
    
}

