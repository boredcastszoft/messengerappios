//
//  GroupManagerVC.swift
//  Boredcast
//
//  Created by Rudolf Dani on 2018. 11. 01..
//  Copyright © 2018. Rudolf Dani. All rights reserved.
//

import UIKit

class GroupManagerVC: UIViewController {

    @IBOutlet weak var createView: UIView!
    @IBOutlet weak var deleteView: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

    }

    @IBAction func selectView(_ sender: UISegmentedControl) {
        
        if sender.selectedSegmentIndex == 0 {
            
            self.createView.isHidden = false
            self.deleteView.isHidden = true
            
        } else if sender.selectedSegmentIndex == 1 {
            
            self.createView.isHidden = true
            self.deleteView.isHidden = false
        }
        
    }
    
    @IBAction func dismissBtnPushed(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }

    
}
