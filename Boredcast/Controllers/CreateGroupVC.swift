//
//  CreateGroupVC.swift
//  Boredcast
//
//  Created by Rudolf Dani on 2018. 11. 06..
//  Copyright © 2018. Rudolf Dani. All rights reserved.
//
import Alamofire
import UIKit

class CreateGroupVC: UIViewController {

    @IBOutlet weak var groupNameText: UITextField!
    @IBOutlet weak var addFriendsText: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func cancelBtnPushed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func createBtnPushed(_ sender: Any) {
        
        let parameters: Parameters = [
            "groupName" : groupNameText.text!,
            "users" : addFriendsText.text!
        ]
        
        if groupNameText.text != nil && groupNameText.text != "" {
            Alamofire.request(MAINURL+"creategroup", method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { (response) in
                print(response)
               /* if let dict = response.result.value as? Dictionary<String, AnyObject> {
                    if let state = dict["state"] as? String{
                        if state == "no" {
                            
                            self.displayMessage(userMessage: "HIBA! A csoport nem jött létre!")
                            return
                        }else {
                            self.displayMessage(userMessage: "A csoport létrejött!")
                            return
                        }
                    }
                }*/
            }
        }else {
            displayMessage(userMessage: "Üres csoport név!")
            
        }
        
       
    }
    
    func displayMessage(userMessage:String) -> Void {
        DispatchQueue.main.async {
            
            let alertController = UIAlertController(title: "Alert", message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                DispatchQueue.main.async {
                    //self.dismiss(animated: true, completion: nil)
                }
            }
            
            alertController.addAction(OKAction)
            self.present(alertController, animated: true)
        }
    }

}
