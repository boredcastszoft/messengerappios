//
//  MessengerVC.swift
//  Boredcast
//
//  Created by Rudolf Dani on 2018. 09. 24..
//  Copyright © 2018. Rudolf Dani. All rights reserved.
//

import UIKit
import Alamofire

class MessengerVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource{

 
    @IBOutlet weak var friendListCollection: UICollectionView!
    @IBOutlet weak var messageText: UITextField!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var friendNameLbl: UILabel!
    @IBOutlet weak var chatTableView: UITableView!

    
    var friendList  = ["kecske"]
    var messages: [Message] = []
    

   
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
     /*   var mess = Message(id: 55, message: "keceeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeske", sender: "Pista", receiver: "asd", sentTime: 211)
        var mess1 = Message(id: 22, message: "kecske", sender: "Feri", receiver: "asd", sentTime: 3)
        var mess2 = Message(id: 11, message: "kecske", sender: "Orbán", receiver: "asd", sentTime: 4)
        var mess3 = Message(id: 222, message: "kecske", sender: "Pista", receiver: "asd", sentTime: 55)
        messages.append(mess)
        messages.append(mess1)
        messages.append(mess2)
        messages.append(mess3)*/
        friendListCollection.delegate = self
        friendListCollection.dataSource = self
        chatTableView.delegate = self
        chatTableView.dataSource = self
        chatTableView.transform = CGAffineTransform(rotationAngle: ( .pi))
        
       
        //self.friendListDownload()

        chatTableView.reloadData()
    }

  
    @IBAction func backBtnPushed(_ sender: Any) {
        
        let time = self.getTime()
        let userName = getUserName()
      
        
        let parameters: Parameters = [
            "userName" : userName,
            "logOutTime" : time
        ]
        
        Alamofire.request(MAINURL+"logout", method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { (response) in
            print(response)
        }
        self.friendList.removeAll()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sendMessageBtnPushed(_ sender: Any) {
        
        let message = messageText.text
        
        let time = self.getTime()
        let receiver = friendNameLbl.text
        let sender = getUserName()
        let messageObject = Message(message: message!, sender: sender, receiver: receiver!, sentTime: time)
                let messageParameter: Parameters = [
                    "message" : message!,
                    "sentTime" : time,
                    "receiver" : receiver!,
                    "sender" : sender
                ]
                
                Alamofire.request(MAINURL+"message", method: .post, parameters: messageParameter, encoding: JSONEncoding.default).responseJSON { (response) in
                    print(response)
                    
                }
        self.messages.append(messageObject)
        messages.sort(by: { $0.sentTime > $1.sentTime })
        messageText.text?.removeAll()
        chatTableView.reloadData()
        
     
    }
    
 
    
    @objc func polling() {
        
        let userName = self.getUserName()
        let parameters: Parameters = [
            "receiver": userName,
           
        ]
       
        Alamofire.request(MAINURL+"polling", method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { (response) in
            print(response)
            
            if let dict = response.result.value as? [Dictionary<String, AnyObject>]{
                
                var _message: String!
                var _sender: String!
                var _receiver: String!
                var _sentTime: CLong!
                
                if let message =  dict[0]["message"] as? String {
                    
                    if message != "Fail" {
                        _message = message
                        
                        if let sender =  dict[0]["sender"] as? String {
                            
                            _sender = sender
                        }
                        if let receiver =  dict[0]["receiver"] as? String {
                            
                            _receiver = receiver
                        }
                        if let sentTime =  dict[0]["sentTime"] as? CLong {
                            
                            _sentTime = sentTime
                        }
                        let message = Message(message: _message, sender: _sender, receiver: _receiver, sentTime: _sentTime)
                        self.messages.append(message)
                    }
                }
                
                
                
                    
                
                }
                self.messages.sort(by: { $0.sentTime > $1.sentTime })
                self.chatTableView.reloadData()
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return friendList.count
    }
   
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FriendListCell", for: indexPath) as? FriendListCell {
            
            let friend = self.friendList[indexPath.row]
            cell.selectedBackgroundView = UIView()
            let kaka = UIColor(red:0.84, green:0.35, blue:0.12, alpha:1.0)
            cell.selectedBackgroundView?.backgroundColor = kaka
            cell.configureCell(friendName: friend)
            
            return cell
        }else {
            return UICollectionViewCell()
        }
    }
    var keksz: String!
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        messages.removeAll()
        
        friendNameLbl.text = friendList[indexPath.row]
        keksz = friendList[indexPath.row]
        //var messages: [Message] = []
        print(friendNameLbl.text!)
        let userName = getUserName()
        let parameters: Parameters = [
            "sender": userName,
            "receiver": friendList[indexPath.row]
        ]
        
        
        
        if friendNameLbl.text != friendList[indexPath.row] {
            self.messages.removeAll()
            Alamofire.request(MAINURL+"getmessages", method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { (response) in
                print(response)
                if let dict = response.result.value as? [Dictionary<String, AnyObject>], dict.count>0{
             
                    var _message: String!
                    var _sender: String!
                    var _receiver: String!
                    var _sentTime: CLong!
                    
                    for x in 0..<dict.count {
                        if let message =  dict[x]["message"] as? String {
                            
                            _message = message
                        }
                     
                        if let sender =  dict[x]["sender"] as? String {
                            
                            _sender = sender
                        }
                        if let receiver =  dict[x]["receiver"] as? String {
                            
                            _receiver = receiver
                        }
                        if let sentTime =  dict[x]["sentTime"] as? CLong {
                            
                            _sentTime = sentTime
                        }
                        let message = Message(message: _message, sender: _sender, receiver: _receiver, sentTime: _sentTime)
                        self.messages.append(message)
                    }
                  
                }
            }
            messages.sort(by: { $0.sentTime < $1.sentTime })
            chatTableView.reloadData()
        }
        
        self.getMessages {
            self.chatTableView.reloadData()
        }
        var timer: Timer!
        timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(polling), userInfo: nil, repeats: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 79, height: 79)
    }
    
   /* func friendListDownload() {
        
        let parameters : Parameters = [
            "username" : userName
        ]
        
        Alamofire.request(MAINURL+"friends", method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { (response) in
            
            if let dict = response.result.value as? [Dictionary<String, AnyObject>], dict.count>0{
                for x in 0..<dict.count {
                    if let friend =  dict[x]["userFriendName"] as? String {
                       
                        
                        self.friendList.append(friend)
                }
                
               
                }
            }
            
        }
        print(friendList)
   
    }*/
    
    func getTime() -> CLong {
        
        let timestamp = Date().currentTimeMillis()
        
        return timestamp!
    }
    
    func getUserName() -> String {
        
        if let userName = UserDefaults.standard.object(forKey: "userName") as? String {
            return userName
        }else {
            return "ERROR!"
        }
    }
    
    func getDateFromTimeStamp(timestamp: CLong) -> String {
        
        let date = Date(timeIntervalSince1970: Double(timestamp))
        
        let dayTimeFormatter = DateFormatter()
        //dayTimeFormatter.dateFormat = "dd MMM YY, hh:mm a"
        dayTimeFormatter.dateFormat = "hh:mm a"
        let timeString = dayTimeFormatter.string(from: date as Date)
        
        return timeString
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
  
        if let cell = tableView.dequeueReusableCell(withIdentifier: "MessageCell", for: indexPath) as? MessageCell {
            
            let message = messages[indexPath.row]
           
            cell.configureTableViewCell(message: message)
            cell.transform = CGAffineTransform(rotationAngle: (.pi))
            
            return cell
        }else {
            
             return UITableViewCell()
        }    
       
    }
    
    @IBAction func fileBtnPushed(_ sender: Any) {
        chatTableView.reloadData()
    }
    
    func getMessages(completed:@escaping downloadCompleted) {
        
        let userName = getUserName()
        let parameters: Parameters = [
            "sender": userName,
            "receiver": keksz
        ]
        
        Alamofire.request(MAINURL+"getmessages", method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { (response) in
            print(response)
            if let dict = response.result.value as? [Dictionary<String, AnyObject>], dict.count>0{
                
                var _message: String!
                var _sender: String!
                var _receiver: String!
                var _sentTime: CLong!
                
                for x in 0..<dict.count {
                    if let message =  dict[x]["message"] as? String {
                        
                        _message = message
                    }
                    
                    if let sender =  dict[x]["sender"] as? String {
                        
                        _sender = sender
                    }
                    if let receiver =  dict[x]["receiver"] as? String {
                        
                        _receiver = receiver
                    }
                    if let sentTime =  dict[x]["sentTime"] as? CLong {
                        
                        _sentTime = sentTime
                    }
                    let message = Message(message: _message, sender: _sender, receiver: _receiver, sentTime: _sentTime)
                    self.messages.append(message)
                }
                self.messages.sort(by: { $0.sentTime > $1.sentTime })
                completed()
            }
        }
        
        chatTableView.reloadData()

        
        
    }
    
    
}
extension Date {
    func currentTimeMillis() -> CLong! {
        
        return CLong(self.timeIntervalSince1970 * 1000)
    }
}
