//
//  SignUpVC.swift
//  Boredcast
//
//  Created by Rudolf Dani on 2018. 09. 29..
//  Copyright © 2018. Rudolf Dani. All rights reserved.
//

import UIKit
import Alamofire

class SignUpVC: UIViewController {

   
    @IBOutlet weak var fullNameText: UITextField!
    @IBOutlet weak var userNameText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var passwordAgainText: UITextField!
    @IBOutlet weak var emailText: UITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func singUpBtnPushed(_ sender: Any) {
        
        if passwordText.text != passwordAgainText.text {
            
            passwordAgainText.text?.removeAll()
            passwordText.text?.removeAll()
            displayMessage(userMessage: "Nem egyeznek a jelszavak")
            
        }else {
            let userName: String = self.userNameText.text!
            let password: String = self.passwordText.text!
            let name: String = self.fullNameText.text!
            
            let parameters: Parameters = [
                "userName" : userName,
                "password" : password,
                "name" : name
            ]
            
            Alamofire.request(MAINURL+"register", method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { (response) in
                print(response)
                
            }
            
            
            dismiss(animated: true, completion: nil)
            
        }
    }
    
    func displayMessage(userMessage:String) -> Void {
        DispatchQueue.main.async {
            
            let alertController = UIAlertController(title: "Alert", message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                
            }
            
            alertController.addAction(OKAction)
            self.present(alertController, animated: true)
        }
    }
    
    
    

}
