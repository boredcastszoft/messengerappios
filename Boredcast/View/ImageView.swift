//
//  Imageview.swift
//  Boredcast
//
//  Created by Rudolf Dani on 2018. 10. 09..
//  Copyright © 2018. Rudolf Dani. All rights reserved.
//

import UIKit

class ImageView: UIImageView {

    override func awakeFromNib() {
        self.layer.cornerRadius = 24.0
        self.clipsToBounds = true
    }

}
