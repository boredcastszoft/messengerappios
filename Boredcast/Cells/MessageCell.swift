//
//  MessageCell.swift
//  Boredcast
//
//  Created by Rudolf Dani on 2018. 10. 15..
//  Copyright © 2018. Rudolf Dani. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {

    
    @IBOutlet weak var friendImg: ImageView!
    @IBOutlet weak var senderLbl: UILabel!
    @IBOutlet weak var messageLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureTableViewCell(message: Message) {
        var time = getDateFromTimeStamp(timestamp: message.sentTime)
        self.senderLbl.text = message.sender
        self.messageLbl.text = message.message
        self.timeLbl.text = time
    }
    
    func getDateFromTimeStamp(timestamp: CLong) -> String {
        var realTimeStamp = timestamp/1000
        let date = Date(timeIntervalSince1970: Double(realTimeStamp))
        
        let dayTimeFormatter = DateFormatter()
        //dayTimeFormatter.dateFormat = "dd MMM YY, hh:mm a"
        dayTimeFormatter.dateFormat = "hh:mm a"
        let timeString = dayTimeFormatter.string(from: date as Date)
        
        return timeString
    }

}
