//
//  FriendListCell.swift
//  Boredcast
//
//  Created by Rudolf Dani on 2018. 09. 27..
//  Copyright © 2018. Rudolf Dani. All rights reserved.
//

import UIKit

class FriendListCell: UICollectionViewCell {
    
    @IBOutlet weak var friendNameLbl: UILabel!
    @IBOutlet weak var friendProfileImg: UIImageView!
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        layer.cornerRadius = 6.0
        
    }
    
    var friendName: String!
    
    func configureCell(friendName: String) {
        
        self.friendName = friendName
        friendNameLbl.text = self.friendName
        
    }
}
